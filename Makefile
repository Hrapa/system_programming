all: lab_2 lab_3 lab_4 lab_5

lab_2:
	$(MAKE) -C lab2/

lab_3:
	$(MAKE) -C lab3/

lab_4:
	$(MAKE) -C lab4/

lab_5:
	$(MAKE) -C lab5/

lab_6:
	$(MAKE) -C lab6/

clean:
	$(MAKE) -C lab2/ clean
	$(MAKE) -C lab3/ clean
	$(MAKE) -C lab4/ clean
	$(MAKE) -C lab5/ clean
	$(MAKE) -C lab6/ clean

