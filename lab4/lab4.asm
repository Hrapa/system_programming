section .bss
buffer:		resb 256

section .data
msg_format db "%d", 0x0A, 0x00
end_msg db "Do you want calculate something else? [y/N]", 0x0A, 0x00

section .text

extern	read, atoi, printf, exit

global	main
main:
	push	255
	push 	buffer
	push	0
	call	read
	add	esp, 12

	sub	eax, 1
	mov	[buffer + eax], byte 0

	push	buffer
	call	atoi
	add	esp, 4

	mov	ebx, eax
	mul	ebx
	mul	ebx

	sub	eax, 2

	mov	ebx, 3
	div	ebx

	add	eax, 4

	push	eax
	push	msg_format
	call	printf
	add	esp, 8

	push	end_msg
	call	printf
	add	esp, 4

	push	255
	push	buffer
	push	0
	call	read

	cmp	[buffer], byte 'y'
	je	main

	push	0
	call	exit
	add	esp, 4

