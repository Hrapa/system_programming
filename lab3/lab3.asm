section .data
error_msg	db "Incorrect number", 0xa
error_len	equ $ - error_msg

msg1	db "It's January!", 0xa 
len1	equ $ - msg1
msg2	db "It's February!", 0xa 
len2	equ $ - msg2
msg3	db "It's March!", 0xa 
len3	equ $ - msg3
msg4	db "It's April!", 0xa 
len4	equ $ - msg4
msg5	db "It's May!", 0xa 
len5	equ $ - msg5
msg6	db "It's June!", 0xa 
len6	equ $ - msg6
msg7	db "It's July!", 0xa 
len7	equ $ - msg7
msg8	db "It's August!", 0xa 
len8	equ $ - msg8
msg9	db "It's September!", 0xa 
len9	equ $ - msg9

section .bss
buffer: resb 256

section .text
global _start
_start:
	push	256
	push	buffer
	push	0
	call	read
	add	esp, 12

	cmp	[buffer + 1], byte 0xA
	jne	_error

	cmp	[buffer], byte 49
	jne	c2
	push	len1
	push	msg1
	push	1
	call	write
	add	esp, 12
	jmp	_exit

c2:
	cmp	[buffer], byte 50
	jne	c3
	push	len2
	push	msg2
	push	1
	call	write
	add	esp, 12
	jmp	_exit

c3:
	cmp	[buffer], byte 51
	jne	c4
	push	len3
	push	msg3
	push	1
	call	write
	add	esp, 12
	jmp	_exit

c4:
	cmp	[buffer], byte 52
	jne	c5
	push	len4
	push	msg4
	push	1
	call	write
	add	esp, 12
	jmp	_exit

c5:
	cmp	[buffer], byte 53
	jne	c6
	push	len5
	push	msg5
	push	1
	call	write
	add	esp, 12
	jmp	_exit

c6:
	cmp	[buffer], byte 54
	jne	c7
	push	len6
	push	msg6
	push	1
	call	write
	add	esp, 12
	jmp	_exit

c7:
	cmp	[buffer], byte 55
	jne	c8
	push	len7
	push	msg7
	push	1
	call	write
	add	esp, 12
	jmp	_exit

c8:
	cmp	[buffer], byte 56
	jne	c9
	push	len8
	push	msg8
	push	1
	call	write
	add	esp, 12
	jmp	_exit

c9:
	cmp	[buffer], byte 57
	jne	_error
	push	len9
	push	msg9
	push	1
	call	write
	add	esp, 12
	jmp	_exit

_error:
	push	error_len
	push	error_msg
	push	1
	call	write
	add	esp, 12
	jmp	_start

_exit:
	push	0
	call	exit
	add	esp, 4

exit:
	push	ebp
	mov	ebp, esp

	mov eax, 1
	mov ebx, [ebp + 8]
	int 0x80

	pop ebp
	ret

read:
	push	ebp
	mov	ebp, esp

	mov	eax, 3
	mov	ebx, [ebp + 8]
	mov	ecx, [ebp + 12]
	mov	edx, [ebp + 16]
	int	0x80
	
	pop ebp
	ret

write:
	push	ebp
	mov	ebp, esp

	mov	eax, 4
	mov	ebx, [ebp + 8]
	mov	ecx, [ebp + 12]
	mov	edx, [ebp + 16]
	int	0x80
	
	pop ebp
	ret

