section .bss
buffer:		resb 256
X:		resd 1
A:		resd 1
B:		resd 1

section .data
x_msg		db "X: ", 0x00
a_msg		db "A: ", 0x00
b_msg		db "B: ", 0x00
msg_format db "RESULT: %d", 0x0A, 0x00

section .text

extern	read, write, strlen, atoi, printf, exit

global	main
main:
	push	x_msg
	call	strlen
	add	esp, 4

	push	eax
	push	x_msg
	push	1
	call	write
	add	esp, 4

	push	255
	push 	buffer
	push	0
	call	read
	add	esp, 12

	sub	eax, 1
	mov	[buffer + eax], byte 0

	push	buffer
	call	atoi
	add	esp, 4

	mov	[X], eax

	push	a_msg
	call	strlen
	add	esp, 4

	push	eax
	push	a_msg
	push	1
	call	write
	add	esp, 4

	push	255
	push 	buffer
	push	0
	call	read
	add	esp, 12

	sub	eax, 1
	mov	[buffer + eax], byte 0

	push	buffer
	call	atoi
	add	esp, 4

	mov	[A], eax

	push	b_msg
	call	strlen
	add	esp, 4

	push	eax
	push	b_msg
	push	1
	call	write
	add	esp, 4

	push	255
	push 	buffer
	push	0
	call	read
	add	esp, 12

	sub	eax, 1
	mov	[buffer + eax], byte 0

	push	buffer
	call	atoi
	add	esp, 4

	mov	[B], eax

	mov	eax, [X]
	mov	ebx, eax
	mul	ebx
	mul	ebx
	mov	ecx, eax

	mov	eax, 2
	mov	ebx, [A]
	mul	ebx

	sub	ecx, eax
	mov	eax, ecx

	mov	ebx, 3
	idiv	ebx

	mov	ecx, eax

	mov	eax, 2
	mov	ebx, [B]
	mul	ebx

	cmp	[X], dword 30
	jge	v2
	add	ecx, ebx
	jmp	end
v2:
	sub	ecx, ebx

end:
	push	ecx
	push	msg_format
	call	printf
	add	esp, 8

	push	0
	call	exit
	add	esp, 4

