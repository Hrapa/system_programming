section .bss
buffer:		resb 256
X:		resd 1

section .data
x_msg		db "X: ", 0x00
msg_format db "%s", 0x0A, 0x00

section .text

extern	read, write, strlen, atoi, printf, exit, itoa

global	main
main:
	push	x_msg
	call	strlen
	add	esp, 4

	push	eax
	push	x_msg
	push	1
	call	write
	add	esp, 4

	push	255
	push 	buffer
	push	0
	call	read
	add	esp, 12

	sub	eax, 1
	mov	[buffer + eax], byte 0

	push	buffer
	call	atoi
	add	esp, 4

	mov	[X], eax

	push	2
	push	buffer
	push	dword [X]
	call	itoa

	push	buffer
	push	msg_format
	call	printf

	push	0
	call	exit
	add	esp, 4

